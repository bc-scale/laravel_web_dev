@extends('layout.master')

@section('title')
Tambah Cast
@endsection

@section('judul')
Tambah Cast
@endsection

@section('content')
<h1>Formulir Pemain Baru</h1>
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="string" name="nama" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message}}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="integer" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror
    <div class="form-group">
        <label>Biodata</label>
        <textarea type="text" name="bio" class="form-control"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror   
    <button type="submit" class="bten btn-primary">Submit</button>
</form>

@endsection