@extends('layout.master')

@section('title')
Daftar Film
@endsection

@section('judul')
Daftar Film
@endsection

@section('content')

<table class="row">
    @forelse ($film as $item)
        <div class="col-4">
            <h3>{{$item->judul}}</h3>
            <p class="card-text"> {{Str::limit($item->ringkasan, 30)}} </p>
            @auth
                <form action="/film/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-info btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn bgtn-danger btn-sm">
                </form>
            @endauth 
            
            @guest
            <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            @endguest

        @empty
            <h1>Data kosong</h1>
        @endforelse
    
    </tbody>
  </table>
  <a href="/film/create/" class="btn btn-secondary ml-10 mt-3">Tambah Film</a>
@endsection