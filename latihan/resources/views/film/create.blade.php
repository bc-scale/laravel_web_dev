@extends('layout.master')

@section('title')
Tambah Film
@endsection

@section('judul')
Tambah Film
@endsection

@section('content')
<h1>Formulir Pendaftaran Film Baru</h1>
<form action="/film" method="POST">
    @csrf
    <div class="form-group">
        <label>judul</label>
        <input type="string" name="judul" class="form-control">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message}}</div>
    @enderror

    <div class="form-group">
        <label>Ringkasan</label>
        <textarea type="text" name="ringkasan" class="form-control"></textarea>
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror 

    <div class="form-group">
        <label>Tahun</label>
        <input type="integer" name="tahun" class="form-control">
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message}}</div>
    @enderror

    <button type="submit" class="bten btn-primary">Submit</button>
</form>

@endsection