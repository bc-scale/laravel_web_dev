@extends('layout.master')

@section('title')
Data Film Detail
@endsection


@section('content')

<h1>{{$film->judul}}</h1>
<p>Release tahun: {{$film->tahun}}</p>
<p>Ringkasan: {{$film->ringkasan}}</p>

<a href="/film/" class="btn btn-info btn-sm">Back</a>

@endsection