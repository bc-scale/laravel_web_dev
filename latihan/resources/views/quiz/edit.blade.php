<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Edit Data</title>

</head>

<body>

<h2>Edit Data Game</h2>

//Code disini

<form action="/game/{{$game->id}}" method="POST">

    @csrf

    @method('PUT')
    
    // name //

    <div class="form-group">

        <label>Nama Game</label>

        <input type="string" name="name" value="{{$game->name}}" class="form-control">

    </div>

    @error('nama')

        <div class="alert alert-danger">{{ $message}}</div>

    @enderror

    // gameplay //

    <div class="form-group">

        <label>Gameplay</label>

        <textarea type="text" name="gameplay" class="form-control">{{$game->gameplay}}</textarea>

    </div>

    @error('gameplay')

    <div class="alert alert-danger">{{ $message}}</div>

    @enderror  

    // developer //
    
    <div class="form-group">

        <label>Nama Developer</label>

        <input type="string" name="developer" value="{{$game->developer}}" class="form-control">

    </div>

    @error('developer')

        <div class="alert alert-danger">{{ $message}}</div>

    @enderror

    // year //

    <div class="form-group">

        <label>Released Year</label>

        <input type="string" name="year" value="{{$game->year}}" class="form-control">

    </div>

    @error('year')

        <div class="alert alert-danger">{{ $message}}</div>

    @enderror

    <button type="submit" class="btn btn-primary">edit</button>

</form>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>


//CRUD Controller

Model

Game.php

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model{
// Code disini
    protected $table = 'game';
    protected $fillable = ['name','gameplay','developer','year'];
}


Controller

GameController.php

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//isi use modelnya

class GameController extends Controller{

// Code disini

    //input & menampilkan data
    //menampilkan list data game   
    public function index()
    {
        $game = Game::all();
        return view('game.index', compact('game'));
    }

    //menampilkan form untuk membuat data game baru
    public function create()
    {
        return view('game.create');
    }

    //menyimpan data baru ke tabel game
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        $game = new Game;

        $game->name = $request->name;
        $game->gameplay = $request->gameplay;
        $game->developer = $request->developer;
        $game->year = $request->year;

        $game->save();

        return redirect('/game');

    }

    //menampilkan detail data game dengan id tertentu
    public function show($game_id)
    {
        $game = Game::where('id',$game_id)->first();
        return view('game.show', compact('game'));
    }

    //edit & update
    //menampilkan form untuk edit game dengan id tertentu
    public function edit($game_id)
    {
        $game = Game::where('id', $game_id)->first();
        return view('game.edit', compact('game'));
    }

    //menyimpan perubahan data game (update) untuk id tertentu
    public function update(Request $request, $game_id)
    {

        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        $game = Game::find($game_id);

        $game->name = $request['name'];
        $game->gameplay = $request['gameplay'];
        $game->developer = $request['developer'];
        $game->year = $request['year'];

        $game->save();

        return redirect('/game');

    }
    // delete
    //	menghapus data game dengan id tertentu
    public function destroy($game_id)
    {
        $game = Game::find($game_id);

        $game->delete();

        return redirect('/game');    
    }

}


Route

web.php

<?php

// CRUD Game

//menampilkan list data game
Route::get('/game','GameController@index'); 

//menampilkan form untuk membuat data game baru
Route::get('/game/create','GameController@create'); 

//menyimpan data baru ke tabel game
Route::post('/game','GameController@store'); 

//menampilkan detail data game dengan id tertentu
Route::get('/game/{game_id}','GameController@show'); 

//menampilkan form untuk edit game dengan id tertentu
Route::get('/game/{game_id}/edit','GameController@edit'); 

//menyimpan perubahan data game (update) untuk id tertentu
Route::put('/game/{game_id}','GameController@update'); 

//	menghapus data game dengan id tertentu
Route::delete('/game/{game_id}','GameController@destroy'); 