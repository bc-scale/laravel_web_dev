<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = 'cast';
    protected $fillable = ['nama','umur','bio'];

    public function castig_film()
    {
        return $this->hasMany('App\Casting_film');
    }
}
