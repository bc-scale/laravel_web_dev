<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Casting_film extends Model
{
    protected $table = 'casting_film';
    protected $fillable = ['cast_id','film_id','nama_peran','deskripsi_peran' ];

    public function film()
    {
        return $this->belongsTo('App\Film');
    }

    public function cast()
    {
        return $this->belongsTo('App\Cast');
    }
}
