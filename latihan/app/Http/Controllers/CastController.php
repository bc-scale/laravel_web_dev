<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{   
    // public function __construct()
    // {
    //     $this->middleware('auth')->except(['index','show']);
        
    // }

    //input & menampilkan data
    //menampilkan list data para pemain film   
    public function index()
    {
        $cast = Cast::all();
        //dd($cast);
        return view('cast.index', compact('cast'));
    }

    //menampilkan form untuk membuat data pemain film baru
    public function create()
    {
        return view('cast.create');
    }

    //menyimpan data baru ke tabel Cast
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $cast = new Cast;

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->save();

        return redirect('/cast');

    }

    //menampilkan detail data pemain film dengan id tertentu
    public function show($cast_id)
    {
        $cast = Cast::where('id',$cast_id)->first();
        return view('cast.show', compact('cast'));
    }

    //edit & update
    //menampilkan form untuk edit pemain film dengan id tertentu
    public function edit($cast_id)
    {
        $cast = Cast::where('id', $cast_id)->first();
        return view('cast.edit', compact('cast'));
    }

    //menyimpan perubahan data pemain film (update) untuk id tertentu
    public function update(Request $request, $cast_id)
    {

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $cast = Cast::find($cast_id);

        $cast->nama = $request['nama'];
        $cast->umur = $request['umur'];
        $cast->bio = $request['bio'];

        $cast->save();

        return redirect('/cast');

    }
    // delete
    //	menghapus data pemain film dengan id tertentu
    public function destroy($cast_id)
    {
        $cast = Cast::find($cast_id);

        $cast->delete();

        return redirect('/cast');    
    }

}
