<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating_comment extends Model
{
    protected $table = 'rating_comment';

    protected $fillable = ['user_id','film_id','rating','isi_komentar'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function film()
    {
        return $this->belongsTo('App\Film');
    }
}
